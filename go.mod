module democouchbase

go 1.14

require (
	github.com/couchbase/gocb/v2 v2.1.3
	github.com/couchbase/gocbcore/v9 v9.0.3
	github.com/go-chi/chi v4.1.2+incompatible
)
