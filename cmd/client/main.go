package main

import (
	"democouchbase/pkg/platform/db"
	"democouchbase/pkg/service/contacts"
	"fmt"
	"github.com/couchbase/gocb/v2"
	"os"
)

func terminateOnError(optionalMessage string, err error) {
	if err != nil {
		fmt.Printf("[Error] %s: %s\n", optionalMessage, err)
		os.Exit(1)
	}
}

func main() {
	const id = "idontexists"
	//const id = "me-someprovider.com_111-111-1111"
	config := getConfig()
	fmt.Printf("Connecting to %s@%s with user %s\n", config.bucketName, config.server, config.userName)
	serviceCollection, err := db.GetCollection(config.server, config.bucketName, config.userName, config.password, config.timeout)
	terminateOnError("Cannot initialize database connection", err)
	fmt.Println("Connected to cluster")
	for {
		getResult, err := serviceCollection.Get(id, &gocb.GetOptions{
			Project: []string{"name"},
			Timeout: config.timeout,
			//RetryStrategy: nil,
		})
		if err != nil {
			fmt.Printf("Error: %s\n", err)
		} else {
			var contact contacts.Contact
			getResult.Content(&contact)
			fmt.Println(contact.Name)
		}
	}
}
