package main

import (
	"flag"
	"time"
)

type config struct {
	server     string
	bucketName string
	userName   string
	password   string
	timeout    time.Duration
}

func getConfig() config {
	server := flag.String("s", "localhost", "Couchbase server")
	bucketName := flag.String("n", "demo", "Couchbase bucket name")
	userName := flag.String("u", "demouser", "Couchbase user name")
	password := flag.String("p", "", "Couchbase password")
	defaultTimeout := flag.Duration("t", 1*time.Second, "Couchbase operations timeout")
	flag.Parse()
	return config{
		server:     *server,
		bucketName: *bucketName,
		userName:   *userName,
		password:   *password,
		timeout:    *defaultTimeout,
	}
}
