package main

import (
	"democouchbase/pkg/platform/db"
	"democouchbase/pkg/service/contacts"
	"fmt"
	"os"
)

func terminateOnError(optionalMessage string, err error) {
	if err != nil {
		fmt.Printf("[Error] %s: %s\n", optionalMessage, err)
		os.Exit(1)
	}
}

func main() {
	config := getConfig()
	fmt.Printf("Connecting to %s@%s with user %s\n", config.bucketName, config.server, config.userName)
	serviceCollection, err := db.GetCollection(config.server, config.bucketName, config.userName, config.password, config.timeout)
	terminateOnError("Cannot initialize database connection", err)
	fmt.Println("Connected to cluster")
	contacts.SetDatasource(serviceCollection)
	startServer()
}
