package main

import (
	"democouchbase/pkg/service/contacts"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/couchbase/gocb/v2"
	"github.com/go-chi/chi"
)

func contactCreateHandler(writer http.ResponseWriter, request *http.Request) {
	payload, err := ioutil.ReadAll(request.Body)
	if err != nil {
		writer.WriteHeader(http.StatusFailedDependency)
		return
	}
	var contact contacts.Contact
	err = json.Unmarshal(payload, &contact)
	if err != nil {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}
	err = contacts.New(contact)
	if err != nil {
		writer.WriteHeader(http.StatusFailedDependency)
		return
	}
	writer.Write([]byte("item created"))
}

func getContactHandler(writer http.ResponseWriter, request *http.Request) {
	contactID := chi.URLParam(request, "contactID")
	if strings.TrimSpace(contactID) == "" {
		writer.WriteHeader(http.StatusBadRequest)
		return
	}
	contact, err := contacts.Get(contactID)
	if err != nil && errors.Is(err, gocb.ErrCollectionNotFound) {
		writer.WriteHeader(http.StatusNotFound)
		return
	}
	if err != nil {
		writer.WriteHeader(http.StatusFailedDependency)
		return
	}
	responseBody, _ := json.Marshal(contact)
	writer.Header().Add("content-type", "application/json")
	writer.Write(responseBody)
}

func startServer() {
	r := chi.NewRouter()
	r.Get("/contact/{contactID}", getContactHandler)
	r.Post("/contact", contactCreateHandler)
	err := http.ListenAndServe(":8080", r)
	terminateOnError("Cannot start web serer", err)
}
