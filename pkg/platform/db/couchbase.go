package db

import (
	"errors"
	"time"

	"github.com/couchbase/gocb/v2"
)

// GetCollection returns a couchbase collection
func GetCollection(server string, bucketName string, user string, password string, timeout time.Duration) (*gocb.Collection, error) {

	cluster, err := gocb.Connect(server, gocb.ClusterOptions{
		Username: user,
		Password: password,
		TimeoutsConfig: gocb.TimeoutsConfig{
			ConnectTimeout: timeout,
			KVTimeout:      timeout,
		},
		//RetryStrategy:        nil,
		//Tracer:               nil,
		//OrphanReporterConfig: gocb.OrphanReporterConfig{},
		CircuitBreakerConfig: gocb.CircuitBreakerConfig{
			//	Disabled:                 false,
			//	VolumeThreshold:          5,
			//	ErrorThresholdPercentage: 2,
			//	SleepWindow:              5 * time.Second,
			//	RollingWindow:            5 * time.Second,
			CompletionCallback: func(err error) bool {
				if err == nil || errors.Is(err, gocb.ErrDocumentNotFound) {
					return true
				}
				return true
			},
		},
	})
	if err != nil {
		return nil, err
	}
	err = cluster.WaitUntilReady(5*time.Second, &gocb.WaitUntilReadyOptions{DesiredState: gocb.ClusterStateOnline})
	if err != nil {
		return nil, err
	}
	bucket := cluster.Bucket(bucketName)
	err = bucket.WaitUntilReady(5*time.Second, &gocb.WaitUntilReadyOptions{DesiredState: gocb.ClusterStateOnline})
	collection := bucket.DefaultCollection()
	return collection, nil
}
