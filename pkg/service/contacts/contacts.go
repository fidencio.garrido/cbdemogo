package contacts

import (
	"fmt"
	"strings"
	"time"

	"github.com/couchbase/gocb/v2"
)

var (
	// TimedOperationResultsHandler handler trigger when
	serviceCollection *gocb.Collection
)

// Contact personal information
type Contact struct {
	Name        string `json:"name"`
	PhoneNumber string `json:"phoneNumber"`
	Email       string `json:"email"`
	Age         int    `json:"age"`
}

// SetDatasource injects the couchbase connection
func SetDatasource(couchbaseCollection *gocb.Collection) {
	serviceCollection = couchbaseCollection
}

// New creates a new contact
func New(contact Contact) error {
	couchbaseID := fmt.Sprintf("%s_%s", strings.Replace(contact.Email, "@", "-", 1), contact.PhoneNumber)
	_, err := serviceCollection.Insert(couchbaseID, contact, &gocb.InsertOptions{
		//Expiry:          0,
		Timeout: 1 * time.Second,
		//RetryStrategy:   nil,
	})
	if err != nil {
		return err
	}
	return nil
}

// Get returns a contact if it exists
func Get(contactID string) (Contact, error) {
	getResult, err := serviceCollection.Get(contactID, &gocb.GetOptions{
		WithExpiry: false, //
		//Project:       []string{"name", "email"},
		//Timeout:       DefaultTimeout,
		//RetryStrategy: nil,
	})
	if err != nil {
		return Contact{}, err
	}
	var contact Contact
	err = getResult.Content(&contact)
	if err != nil {
		return Contact{}, err
	}
	return contact, nil
}
